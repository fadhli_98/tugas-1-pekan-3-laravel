<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.register');
    }


    public function welcome(Request $request)
    {
        //dd($request->all());
        $firstname = $request['first_name'];
        $lastname = $request['last_name'];
        $gender = $request['kl'];
        $nationality = $request['nationality'];
        $languagespoken = $request['Language_spoken'];
        $biodata = $request['Bio'];

        return view('page.join', compact("firstname", "lastname", "gender", "nationality", "languagespoken", "biodata"));

    }
}
